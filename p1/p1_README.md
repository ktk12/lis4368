# LIS4368: Advanced Web Applications Development

## Kyle Kane

### Project 1 # Requirements:

*3 Things:*

1. Clone student files
2. Update all tags and form controls
3. Add data validation using regular expressions


#### Assignment Screenshots:

*Screenshot of correct data validation*:

![Valid Data](img/valid.png)

*Screenshot of data validation*:

![NonValid Data](img/notvalid.png)

