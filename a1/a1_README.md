# LIS4368: Advanced Web Applications Development

## Kyle Kane

### Assignment 1 # Requirements:


*Three Parts:*

1. Distributed Version Control with Git and Bitbucket, and Development Enviroments
2. Java/JSP/Servlet Development Installation
3. Chapter Questions 1-4


#### README.md file should include the following items:

* Screenshot of running Java Hello(#1 above)
* Screenshot of running http://localhost.9999(#2 above, step #4(b) in tutorial)
* Git commands with short descriptions
* Bitbucket repo links: a) this assignment and b) the complete tutorial above(bitbucketstationlocation and 	team quotes)


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: This command creates an empty Git repository
2. git status: This command displays the state of the working directory and the staging area
3. git add: This command adds file contents to the index
4. git commit: This command commits the staged snapshot to the project history
5. git push: This command sends your committed changes to bitbucket
6. git pull: This command pulls a file from a remote repo into your local repo
7. git clone: This command creates a copy of a local repository

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of running http://localhost.9999*:

![localhost.9999](img/tomcat.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ktk12/bitbucketstationlocation)

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ktk12/myteamquotes)
