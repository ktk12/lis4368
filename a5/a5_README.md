# LIS4368: Advanced Web Applications Development

## Kyle Kane

### Assignment 4 # Requirements:

*4 Things:*

1. Clone Student files
2. Alter WEB-INF and META-INF files 
3. Alter files to prevent SQL injection
4. Provide server-side validation


#### Assignment Screenshots:

*Correct Validation*:

![Correct Validation](img/valid.png)

*Passed*:

![Passed](img/passed.png)

*mysql*:

![mysql](img/mysql.png)


