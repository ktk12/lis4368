# LIS4368: Advanced Web Applications Development

## Kyle Kane

### Assignment 2 # Requirements:



*Four things:*

1. Download and Install mySQL
2. Follow mySQL tutorial to create new user
3. Finish Tomcat tutorial - Develop and Deploy a webapp
4. Link to finish tutorital links


#### Assignment Screenshot:

*Query Results*:

![Query Results Screenshot](img/queryresults.png)


#### Tutorial Links:

*Directory:*
[hello](http://localhost:9999/hello)

*HelloHome:*
[HelloHome](http://localhost:9999/hello/index.html)

*Invokes HelloServlet:*
[sayhello](http://localhost:9999/hello/sayhello)

*QueryBook:*
[querybook](http://localhost:9999/hello/querybook.html)

*SayHi:*
[sayhi](http://localhost:9999/hello/sayhi)

