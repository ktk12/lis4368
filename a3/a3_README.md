# LIS4368: Advanced Web Applications Development

## Kyle Kane

### Assignment 3 # Requirements:


*Sub-Heading:*

1. Design a database that follows the busines rules provided 
2. All tables must have notes
3. All tables must have at least 10 unique records per table
4. Database must foward engineer


#### Assignment Screenshots:

*Screenshot ERD*:

![A3 ERD](img/A3.png)

#### Assignment Links:


[A3 MWB](a3.mwb)

[A3 SQL](a3.sql)