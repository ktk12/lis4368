# LIS4368: Advanced Web Applications Development

## Kyle Kane

### Project 2 # Requirements:

*3 Things:*

1. Clone student files
2. Update all tags and form controls
3. Add data validation using regular expressions
4. Create Customer, Modify and CustomerServlet


#### Assignment Screenshots:

*Screenshot of Customer Display*:

![Customer Display](img/display.png)

*Screenshot of Update Page*:

![Update](img/update.png)

*Screenshot of Edit*:

![Edit](img/updatesql.png)

*Screenshot of Delete*:

![Delete](img/deletesql.png)