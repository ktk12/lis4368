# LIS4368: Advanced Web Applications Development

## Kyle Kane

####Course Work Links:

1. [A1 README.md](a1/a1_README.md)

	* Setup Git and Bitbucket 
	* Install JDK
	* Install Apache Tomcat
	* Git Commands w/ short descriptions
	* Completed tutorial repos

2. [A2 README.md](a2/a2_README.md)

	* Download and Install mySQL DBMS
	* Complete tutorial to create new user
	* Finish Tomcat tutorial to develop and deploy a webapp
	* Provide tutorial links

3. [A3 README.md](a3/a3_README.md)
	
	* Design a database that follows the provided business rules
	* Provide screenshot of ERD
	* Provide links to .mwb and .sql files

4. [P1 README.md](p1/p1_README.md)

	* Clone Lis4368 Student Files
	* Modify index.jsp meta tags
	* Change title, navigation links, and header tags appropriately
	* Add form controls to match attributes
	* Use data validation with regular expressions
	* Push to bitbucket repo

5. [A4 README.md](a4/a4_README.md)
	
	* Learn and implement MVC framework
	* Create and alter Model layer
	* Create and alter View layer
	* Create and alter Controller layer

6. [A5 README.md](a5/a5_README.md)

	* Clone starter files
	* Learn and alter files to prevent SQL injection
	* Learn and alter files to prevent cross-side scripting
	* Push files to bitbucket

7. [P2 README.md](p2/p2_README.md)

	* Clone Starter files
	* Create Update, Delete tags
	* Create Customer.jsp
	* Create CustomerServlet.java
	* Create Modify.java that creates display table